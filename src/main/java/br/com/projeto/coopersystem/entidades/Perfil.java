package br.com.projeto.coopersystem.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity(name = "TB_PERFIL")
public class Perfil implements Serializable {

	private static final long serialVersionUID = -6556964151804943472L;

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "DESCRICAO")
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
