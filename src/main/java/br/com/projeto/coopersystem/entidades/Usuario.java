package br.com.projeto.coopersystem.entidades;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity(name = "TB_USUARIO")
@EntityListeners(AuditingEntityListener.class)
public class Usuario implements Serializable {

    private static final long serialVersionUID = -7682784373836941655L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 100)
    private String nome;


    @Column(nullable = false)
    private String cpf;

    @Column
    private String email;

    @Column
    private String password;

    @OneToMany
    @JoinColumn(name = "id_usuario")
    private List<Email> emails;


    @OneToMany
    @JoinColumn(name = "id_usuario")
    private List<Endereco> enderecos;

    @OneToMany
    @JoinColumn(name = "id_usuario")
    private List<Telefone> telefones;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "rl_usuario_perfil",
            joinColumns = {@JoinColumn(name = "id_usuario", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "id_perfil", referencedColumnName = "id")})
    private Set<Perfil> perfis;


    public List<Email> getEmails() {
        return emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public Set<Perfil> getPerfis() {
        return perfis;
    }

    public void setPerfis(Set<Perfil> perfis) {
        this.perfis = perfis;
    }


    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

