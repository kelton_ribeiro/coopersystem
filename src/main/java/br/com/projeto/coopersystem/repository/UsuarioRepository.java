package br.com.projeto.coopersystem.repository;

import br.com.projeto.coopersystem.entidades.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    @Query("select u from br.com.projeto.coopersystem.entidades.Usuario u where  u.email = :email and u.password = :senha  ")
    public Usuario autenticar(@Param("email") String email, @Param("senha") String senha);




}
