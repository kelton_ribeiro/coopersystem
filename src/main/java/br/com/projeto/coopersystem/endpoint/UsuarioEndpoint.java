package br.com.projeto.coopersystem.endpoint;

import br.com.projeto.coopersystem.entidades.Usuario;
import br.com.projeto.coopersystem.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/usuario")
public class UsuarioEndpoint {

    @Autowired
    private UsuarioService usuarioService;


    @GetMapping("/{id}")
    public Optional<Usuario> buscar(@PathVariable Long id) {
        return usuarioService.buscar(id);
    }


    @GetMapping("/{email}/{senha}")
    public Usuario autenticar(@PathVariable("email") String email, @PathVariable("senha") String senha){
        return usuarioService.autenticar(email, senha);
    }

    @PostMapping
    public Usuario salvar(@RequestBody Usuario usuario) {
        return usuarioService.salvar(usuario);

    }


}
