package br.com.projeto.coopersystem.vo;

import java.util.Date;

public class ProcessoVO {
	
	private Long idProcesso;
	private String numeroProcesso;
	private Date dataPublicacao;
	private String pagina;
	private String  relator;
	private String agravante;
	private String agravado;
	private String conteudo;

	public Long getIdProcesso() {
		return idProcesso;
	}

	public void setIdProcesso(Long idProcesso) {
		this.idProcesso = idProcesso;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public Date getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(Date dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	public String getRelator() {
		return relator;
	}

	public void setRelator(String relator) {
		this.relator = relator;
	}

	public String getAgravante() {
		return agravante;
	}

	public void setAgravante(String agravante) {
		this.agravante = agravante;
	}

	public String getAgravado() {
		return agravado;
	}

	public void setAgravado(String agravado) {
		this.agravado = agravado;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	
	
	
	
	

}
