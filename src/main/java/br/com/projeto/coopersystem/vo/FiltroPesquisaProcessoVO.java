package br.com.projeto.coopersystem.vo;

import java.util.Date;

/**
 * Created by kelton on 15/07/17.
 */
public class FiltroPesquisaProcessoVO {

    private Date liberacao;
    private String processo;
    private String intimado;
    private String advogado;

    public Date getLiberacao() {
        return liberacao;
    }

    public void setLiberacao(Date liberacao) {
        this.liberacao = liberacao;
    }

    public String getProcesso() {
        return processo;
    }

    public void setProcesso(String processo) {
        this.processo = processo.replace(".","").replace("-","");
    }

    public String getIntimado() {
        return intimado;
    }

    public void setIntimado(String intimado) {
        this.intimado = intimado;
    }

    public String getAdvogado() {
        return advogado;
    }

    public void setAdvogado(String advogado) {
        this.advogado = advogado;
    }

}
