package br.com.projeto.coopersystem.vo;


import br.com.projeto.coopersystem.entidades.Perfil;

public class UsuarioVO {

	private Long id;
	private Perfil perfil;
	private String nome;
	private String email;
	private String telefone;
	private String cpf;

	public UsuarioVO() {
	}

	public UsuarioVO(UsuarioVOBuilder builder) {
		this.id = builder.id;
		this.nome = builder.nome;
		this.email = builder.email;
		this.perfil = builder.perfil;
		this.telefone = builder.telefone;
		this.cpf = builder.cpf;
	}

	public static class UsuarioVOBuilder {
		private Long id;
		private Perfil perfil;
		private String nome;
		private String email;
		private String telefone;
		private String cpf;

		public UsuarioVOBuilder(String nome, String cpf) {
			this.nome = nome;
			this.cpf = cpf;
		}

		public UsuarioVOBuilder id(Long id) {
			this.id = id;
			return this;
		}

		public UsuarioVOBuilder nome(String nome) {
			this.nome = nome;
			return this;
		}

		public UsuarioVOBuilder Perfil(Perfil perfil) {
			this.perfil = perfil;
			return this;
		}

		public UsuarioVOBuilder email(String email) {
			this.email = email;
			return this;
		}

		public UsuarioVOBuilder telefone(String telefone) {
			this.telefone = telefone;
			return this;
		}

		public UsuarioVO build() {
			return new UsuarioVO(this);
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
