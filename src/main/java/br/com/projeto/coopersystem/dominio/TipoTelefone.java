package br.com.projeto.coopersystem.dominio;


public enum TipoTelefone {

    RESIDENCIAL, CELULAR, TRABALHO, RECADO;
}
