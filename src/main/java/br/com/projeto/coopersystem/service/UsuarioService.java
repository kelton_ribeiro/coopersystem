package br.com.projeto.coopersystem.service;

import br.com.projeto.coopersystem.entidades.Usuario;
import br.com.projeto.coopersystem.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Optional<Usuario> buscar(Long id){
        Optional<Usuario> usuario =  usuarioRepository.findById(id);
        return usuario;
    }


    public Usuario autenticar(String email, String senha){
        Usuario usuario = usuarioRepository.autenticar(email,senha);
        return usuario;
    }


    @Transactional
    public Usuario salvar(Usuario usuario){
        return usuarioRepository.save(usuario);
    }

}
